/*
 * Code to write to the 1602 LCD display.
 * LCD configured:           TQFP              DIP
 *     D4 - D7 to PB0 - PB3  12, 13, 14, 15    14, 15, 16, 17
 *     RS      to PB4        16                18
 *     EN      to PB5        17                19
 *     A       to 5v
 *     K       to GND
 */

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "lcd_1602.h"

// if defined, compile code to check ranges of some params
//#define CHECK_PARAM

// number of lines in display
#define NUMLINES	2	// only the 1602 display

//******
// defines to indicate the type of data in "lcd_write()" - command or data
//******

#define WRITE_CMD       0
#define WRITE_DATA      1

//******
// Commands for the 1602 display (taken from Adafruit library)
//******

#define LCD_CLEARDISPLAY    0x01 	//!< Clear display, set cursor position to zero
#define LCD_RETURNHOME      0x02 	//!< Set cursor position to zero
#define LCD_ENTRYMODESET    0x04 	//!< Sets the entry mode
#define LCD_DISPLAYCONTROL  0x08 	//!< Controls the display; does stuff like turning it off and on
#define LCD_CURSORSHIFT     0x10 	//!< Lets you move the cursor
#define LCD_FUNCTIONSET     0x20 	//!< Used to send the function to set to the display
#define LCD_SETCGRAMADDR    0x40 	//!< Used to set the CGRAM (character generator RAM) with characters
#define LCD_SETDDRAMADDR    0x80 	//!< Used to set the DDRAM (Display Data RAM)

// flags for display entry mode
#define LCD_ENTRYRIGHT      0x00 	//!< Used to set text to flow from right to left
#define LCD_ENTRYLEFT       0x02 	//!< Uset to set text to flow from left to right
#define LCD_ENTRYSHIFTINCREMENT  0x01 	//!< Used to 'right justify' text from the cursor
#define LCD_ENTRYSHIFTDECREMENT  0x00 	//!< Used to 'left justify' text from the cursor

// flags for display on/off control
#define LCD_DISPLAYON       0x04 	//!< Turns the display on
#define LCD_DISPLAYOFF      0x00 	//!< Turns the display off
#define LCD_CURSORON        0x02 	//!< Turns the cursor on
#define LCD_CURSOROFF       0x00 	//!< Turns the cursor off
#define LCD_BLINKON         0x01 	//!< Turns on the blinking cursor
#define LCD_BLINKOFF        0x00 	//!< Turns off the blinking cursor

// flags for display/cursor shift
#define LCD_DISPLAYMOVE     0x08 	//!< Flag for moving the display
#define LCD_CURSORMOVE      0x00 	//!< Flag for moving the cursor
#define LCD_MOVERIGHT       0x04 	//!< Flag for moving right
#define LCD_MOVELEFT        0x00 	//!< Flag for moving left

// flags for function set
#define LCD_8BITMODE        0x10 	//!< LCD 8 bit mode
#define LCD_4BITMODE        0x00 	//!< LCD 4 bit mode
#define LCD_2LINE           0x08 	//!< LCD 2 line mode
#define LCD_1LINE           0x00 	//!< LCD 1 line mode
#define LCD_5x10DOTS        0x04 	//!< 10 pixel high font mode
#define LCD_5x8DOTS         0x00 	//!< 8 pixel high font mode

//******
// Define connections to the display.
//
// LCDPORT  the data port for the display
// LCDDIR   is the direction register for LCDPORT
// RS        is RS pin number in LCDPORT
// EN        is EN pin number in LCDPORT
//
// The 4 data bits are assumed to be the 0 - 3 pins of LCDPORT.
//******

#define LCDDIR  DDRB        // Define LCD data direction port
#define LCDPORT PORTB       // Define LCD data port, the 4 data bits assumed to be low bits of the port
#define RS PB4              // Define Register Select pin (RS)
#define EN PB5              // Define Enable signal pin (EN)

// state of the display: display on/off, cursor on/off, blink on/off
static uint8_t DisplayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;


///////////////////////////////////////////////////////////////////////////////
// Pulse the EN bit high for >450ns
///////////////////////////////////////////////////////////////////////////////

#ifdef JUNK
static void lcd_pulse_en(void)
{
    LCDPORT |= (1 << EN);		// EN high
    _delay_us(1);			// 1000ns delay
    LCDPORT &= ~(1 << EN);		// EN low
}
#endif

// save  a few bytes by inlining this
#define lcd_pulse_en()  {LCDPORT |= (1 << EN); _delay_us(1); LCDPORT &= ~(1 << EN);}

///////////////////////////////////////////////////////////////////////////////
// Send command or data to the LCD.
//   cmd   the command or data to write
//   type  WRITE_CMD or WRITE_DATA
///////////////////////////////////////////////////////////////////////////////

static void lcd_write(uint8_t cmd, uint8_t type)
{
    if (type == WRITE_DATA)
        LCDPORT |= (1 << RS);           // RS=1, data write
    else
        LCDPORT &= ~(1 << RS);          // RS=0, command write

    LCDPORT = (LCDPORT & 0xF0) | (cmd >> 4);    // sending upper nybble
    lcd_pulse_en();
    LCDPORT = (LCDPORT & 0xF0) | (cmd & 0x0F);  // sending lower nybble
    lcd_pulse_en();

    _delay_us(50);				// commands need > 37us to settle
}

///////////////////////////////////////////////////////////////////////////////
// Initialize the LCD.
//
// Wait for 50ms, Power on initialization time for LCD1602.
// Send 0x02 command which initializes LCD 16x2 in 4-bit mode.
// Send 0x28 command which configures LCD in 2-line, 4-bit mode and 5x8 dots.
// Send any Display ON command (0x0E, 0x0C)
// Send 0x06 command (increment cursor)
///////////////////////////////////////////////////////////////////////////////

void lcd_init(void)
{
    _delay_ms(50);                          // LCD Power ON delay

    LCDDIR |= 0x0F + (1 << RS) + (1 << EN); // Make LCDPORT low 4 bits and RS+EN pins output

    lcd_write(0x02, WRITE_CMD);           // send for 4 bit initialization of LCD
    lcd_write(0x28, WRITE_CMD);           // 2 line, 5*7 matrix in 4-bit mode
    lcd_write(0x0c, WRITE_CMD);           // Display on cursor off
    lcd_write(0x06, WRITE_CMD);           // Increment cursor (shift cursor to right)
    lcd_clear();
}

///////////////////////////////////////////////////////////////////////////////
// Clear the LCD display.
///////////////////////////////////////////////////////////////////////////////

void lcd_clear(void)
{
    lcd_write(LCD_CLEARDISPLAY, WRITE_CMD);	// Clear display
    _delay_ms(2);
}

///////////////////////////////////////////////////////////////////////////////
// Move the cursor to 0,0.
///////////////////////////////////////////////////////////////////////////////

void lcd_home(void)
{
    lcd_write(LCD_RETURNHOME, WRITE_CMD);	// Clear display
    _delay_ms(2);
}

///////////////////////////////////////////////////////////////////////////////
// Send character to LCD.
//
// First send Higher nibble of data.
// Make RS pin high, RS=1 (data reg.)
// Make RW pin low, RW=0 (write operation) or connect it to ground.
// Give High to Low pulse at Enable (E).
// Send lower nibble of data.
// Give High to Low pulse at Enable (E).
///////////////////////////////////////////////////////////////////////////////

void lcd_char(uint8_t data)
{
    lcd_write(data, WRITE_DATA);
}

///////////////////////////////////////////////////////////////////////////////
// Send string to LCD.
///////////////////////////////////////////////////////////////////////////////

void lcd_string (char *str)
{
    while (*str)
        lcd_char(*str++);
}

///////////////////////////////////////////////////////////////////////////////
// Send string to LCD with xy position.
//     row  row position
//     col  column position
//     str  the string to display
///////////////////////////////////////////////////////////////////////////////

void lcd_string_xy(uint8_t col, uint8_t row, char *str)
{
    lcd_set_cursor(col, row);	// set cursor to required position
    lcd_string(str);            // Call LCD string function
}

///////////////////////////////////////////////////////////////////////////////
// Set cursor position.
//     col  column position
//     row  row position
///////////////////////////////////////////////////////////////////////////////

void lcd_set_cursor(uint8_t col, uint8_t row)
{
    uint8_t offsets[] = {0x00, 0x40};

#ifdef CHECK_PARAM
    if (row > (NUMLINES - 1))
        row = NUMLINES;
#endif

    lcd_write(LCD_SETDDRAMADDR | (col + offsets[row]), WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Turn display on or off.
///////////////////////////////////////////////////////////////////////////////

void lcd_display_on(void)
{
    DisplayControl |= LCD_DISPLAYON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

void lcd_display_off(void)
{
    DisplayControl &= ~LCD_DISPLAYON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Turn the "underline" cursor on or off.
///////////////////////////////////////////////////////////////////////////////

void lcd_cursor_on(void)
{
    DisplayControl |= LCD_CURSORON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

void lcd_cursor_off(void)
{
    DisplayControl &= ~LCD_CURSORON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Turn blinking cursor on or off.
// This should not be confused with the "underline" cursor.
// The blinking cursor and the underline cursor can be combined,
///////////////////////////////////////////////////////////////////////////////

void lcd_blink_on(void)
{
    DisplayControl |= LCD_BLINKON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

void lcd_blink_off(void)
{
    DisplayControl &= ~LCD_BLINKON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Create a custom CG character.
///////////////////////////////////////////////////////////////////////////////

void lcd_create_char(uint8_t index, uint8_t *chardata)
{
#ifdef CHECK_PARAM
    index &= 0x7;				// we only have 8 nums 0-7
#endif

    lcd_write(LCD_SETCGRAMADDR | (index << 3), WRITE_CMD);
    for (int i=0; i < 8; ++i)
        lcd_write(*chardata++, WRITE_DATA);
}

///////////////////////////////////////////////////////////////////////////////
// Scroll whole display left and right.
///////////////////////////////////////////////////////////////////////////////

void lcd_scroll_left(void)
{
    lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT, WRITE_CMD);
}

void lcd_scroll_right(void)
{
    lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT, WRITE_CMD);
}
