// Code to handle a queue of EVENTs.

#include <avr/io.h>
#include "events.h"

// The array holding events
EVENT Events[NUM_EVENTS];

// fore and aft indices for the circular queue
uint8_t ForeSlot = 0;
uint8_t AftSlot = 0;


//-------------------------------------
// Push an EVENT onto the event queue.
//     event     a small integer representing the event
//-------------------------------------

void event_add(EVENT event)
{
#ifdef EVENT_REENTRANT
    cli();
#endif

    Events[ForeSlot] = event;
    ++ForeSlot;
    if (ForeSlot >= NUM_EVENTS)
        ForeSlot = 0;
#ifdef JUNK
    if (ForeSlot == AftSlot)
        // BANG, queue full
#endif

#ifdef EVENT_REENTRANT
        sei();
#endif
}

//-------------------------------------
// Return the next EVENT in the queue.
// If the queue is empty, returns EVENT_None.
//-------------------------------------

EVENT event_get(void)
{
#ifdef EVENT_REENTRANT
    cli();
#endif

    if (ForeSlot == AftSlot)
    {	// queue is empty
#ifdef EVENT_REENTRANT
        sei();
#endif

        return Event_None;
    }

    EVENT event = Events[AftSlot];
    if (++AftSlot > NUM_EVENTS)
        AftSlot = 0;

#ifdef EVENT_REENTRANT
    sei();
#endif

    return event;
}

//-------------------------------------
// Clear all events.
//-------------------------------------

void event_clear(void)
{
#ifdef EVENT_REENTRANT
    cli();
#endif

    AftSlot = ForeSlot = 0;

#ifdef EVENT_REENTRANT
    sei();
#endif
}
