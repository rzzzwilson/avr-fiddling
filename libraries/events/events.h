// Code to handle EVENTS.
//
// Define the inteface.

#ifndef EVENTS_H
#define EVENTS_H

#include <stddef.h>

#define NUM_EVENTS 	5

typedef uint8_t EVENT;

#define Event_None	0

void event_add(EVENT event);
EVENT event_get(void);
void event_clear(void);


#endif
