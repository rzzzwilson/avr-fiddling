LCD_1602
========

A library to write to the 1602 LCD display.

Original code from:

https://www.electronicwings.com/avr-atmega/interfacing-lcd-16x2-in-4-bit-mode-with-atmega-16-32-

and the Adafruit LiquidCrystal library.

Test Connections
----------------

The connections for the test code in main.c are::

* Vss     to GND
* Vdd     to +5v
* V0      to variable voltage (0-5)
* RS      to PB4        (chip pin 18)
* RW      to GND
* EN      to PB5        (chip pin 19)
* D4 - D7 to PB0 - PB3  (chip pins 14, 15, 16, 17)
* A       to +5v
* K       to GND

Connection definitions
----------------------

The pin connections to the display must be #defined in the *lcd_1602.h* file.
The definitions to be used are::

    PORT_RS     \ port/pin for RS
    PIN_RS      /
    PORT_EN     \ port/pin for EN
    PIN_EN      /
    PORT_RW     may be left undefined if RW not used
    PIN_RW      may be left undefined if RW not used
   
    PORT_DATA   the port for all 4 data pins
    PIN_D4      pin for D4
    PIN_D5      pin for D5
    PIN_D6      pin for D6
    PIN_D7      pin for D7
