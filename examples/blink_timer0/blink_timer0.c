// this code sets up a timer0 for 4ms @ 16Mhz clock cycle
// an interrupt is triggered each time the interval occurs.
// toggle an LED to ground on PC0 (pin 23) in the interrupt routine
//
// This seems to be running a bit slow (factor of 2?).

#include <avr/io.h> 
#include <avr/interrupt.h>


volatile unsigned int counter = 0;


int main(void)
{
    DDRC |= _BV(DDC0);                   // set pin PC0 to output
    TCCR0A |= (1 << WGM01);              // Set the Timer Mode to CTC
    OCR0A = 0xF9;                        // Set the value that you want to count to
    TIMSK0 |= (1 << OCIE0A);             // Set the ISR COMPA vect
    PORTC |= (1 << PC0);                 // set the led on PC0 ON
    sei();                               // enable interrupts
    TCCR0B |= (1 << CS02) + (1 << CS00); // set prescaler to 1024 and start the timer

    while (1)
    {
        // when count is 64, toggle LED
        if (counter > 63)                // after 64 overflows
        {                                // ie, about 500ms
            counter = 0;
            PORTC ^= (1 << PC0);         // toggle the led on PC0
        }
    }
}

ISR (TIMER0_COMPA_vect)                  // timer0 overflow interrupt
{
    ++counter;                           // just add 1 to overflow counter
}
