// Code to debounce a single button.
//
// Define the inteface.
//
// From:
// http://www.ganssle.com/debouncing-pt2.htm
//
// Much hacked, so any errors are mine!

#ifndef BUTTONS_H
#define BUTTONS_H

#define NUM_BUTTONS 	5

typedef void (BTN_HANDLER)(void);

// set up button
int8_t Btn_connect(volatile uint8_t* port, uint8_t pin, BTN_HANDLER *handler);
// read all buttons and call handlers on BUTTONUP
void Btn_read(void);
// read the button associated with "index" and call handler if BUTTONUP
void Btn_read_index(uint8_t index);

#endif
