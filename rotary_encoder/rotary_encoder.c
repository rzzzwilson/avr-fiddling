// Robust Rotary encoder reading
//
// From:
// https://www.best-microcontroller-projects.com/rotary-encoder.html
//
// Much hacked, so any errors are mine!

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include "rotary_encoder.h"

//-------------------------------------
// Read a value from the rotary encoder.
// Returns:
//     -1   one click CCW
//     0    no movement
//     1    one click CW
// If the "sense" of the rotation is backwards invert the assignments
// to RE_Clock and RE_Data pins in "rotary_encoder.h".
//-------------------------------------
int8_t RE_read(void)
{
    static char valid_code[] = {0,1,1,0,1,0,0,1,1,0,0,1,0,1,1,0};
    static uint16_t store = 0;
    static uint16_t prevNextCode = 0;

    // put next two pin readings into the shift buffer
    prevNextCode <<= 2;
    if (RE_Pin & (1 << RE_Data))
        prevNextCode |= 0x02;
    if (RE_Pin & (1 << RE_Clock))
        prevNextCode |= 0x01;
    prevNextCode &= 0x0f;

    // If valid then store as 16 bit data.
    if (valid_code[prevNextCode])
    {
        store <<= 4;
        store |= prevNextCode;
        if ((store & 0xff) == 0x2b)
            return -1;
        if ((store & 0xff) == 0x17)
            return 1;
    }

    return 0;
}
