/*
 * Test code to exercise the lcd_1602.c code.
 */

#include <stdio.h> 
#include <avr/io.h>
#include <util/delay.h>

#include "lcd_1602.h"


// if defined will minimize memory used in main.c to check library size
//#define CHECK_LIB_SIZE

#ifndef CHECK_LIB_SIZE
uint8_t cg_data0[] = {0x04, 0x0E, 0x0E, 0x0E, 0x1F, 0x00, 0x04, 0x00};
uint8_t cg_data1[] = {0x03, 0x07, 0x0F, 0x1F, 0x1E, 0x1C, 0x18, 0x00};
#endif

///////////////////////////////////////////////////////////////////////////////
// Run through many tests of 1602 LCD operation.
///////////////////////////////////////////////////////////////////////////////

int main(void)
{
#ifndef CHECK_LIB_SIZE
//    char buff[6];

    lcd_init();                     // Initialization of LCD

    while (1)
    {
        lcd_clear();
        lcd_string(" lcd_1602 test");   // Write string on 1st line of LCD
        _delay_ms(2000);

        lcd_clear();			// test lcd_set_cursor()
        lcd_set_cursor(5, 0);
        lcd_string(". (5,0)");
	lcd_string_xy(0, 1, "Position @ (0,5)");
        _delay_ms(2000);
        lcd_clear();
        lcd_set_cursor(3, 1);
        lcd_string(". (3,1)");
	lcd_string_xy(0, 0, "Position @ (3,1)");
        _delay_ms(2000);
    
        lcd_clear();			// test the underline cursor
        lcd_string_xy(0, 0, "Underline cursor");
        lcd_string_xy(0, 1, "0123456789ABCDEF");
	lcd_home();
	lcd_cursor_on();
        for (int i=0; i < 16; ++i)
        {
            lcd_set_cursor(i, i&1);
	    _delay_ms(500);
        }
	lcd_cursor_off();
    
        lcd_clear();			// test the block blinking cursor
        lcd_string_xy(0, 0, "  Block Cursor  ");
        lcd_string_xy(0, 1, "  ^");
	lcd_set_cursor(2, 0);
        lcd_blink_on();
        _delay_ms(1500);
        lcd_string_xy(0, 1, "   ^");
	lcd_set_cursor(3, 0);
        _delay_ms(1500);
        lcd_string_xy(0, 1, "    ^");
	lcd_set_cursor(4, 0);
        _delay_ms(1500);
        lcd_blink_off();
    
        lcd_clear();			// test turning display on/off
        lcd_string_xy(0, 0, "     Display    ");
        lcd_string_xy(0, 1, "     on/off     ");
        for (int i=0; i < 3; ++i)
        {
            lcd_display_on();
            _delay_ms(500);
            lcd_display_off();
            _delay_ms(500);
        }
        lcd_display_on();
    
//        lcd_clear();	// lcd_clear() *before* lcd_create_char() screws up
        lcd_create_char(0, cg_data0);		// test CG characters
        lcd_create_char(1, cg_data1);
        lcd_clear();
        lcd_char(0);
        lcd_char(1);
//        lcd_string_xy(0, 0, "\x00\x01");	// this doesn't work
//        buff[0] = 0;				// also doesn't work
//        buff[1] = 1;
//        buff[2] = '\0';
//        lcd_string_xy(0, 0, buff);
        lcd_string_xy(8, 0, "CG chars");
        lcd_string_xy(10, 1, "012345");
        _delay_ms(2000);

        lcd_string_xy(0, 1, "Scroll R");	// test scrolling display right
        for (int i=0; i < 5; ++i)
        {
            lcd_scroll_right();
            _delay_ms(1000);
        }
    
        lcd_string_xy(10, 1, "ABCDEFGHIJKLMNOP");// update off-screen chars
        _delay_ms(1000);

        lcd_string_xy(0, 1, "Scroll L");	// test scrolling display left
        for (int i=0; i < 10; ++i)
        {
            lcd_scroll_left();
            _delay_ms(1000);
        }

	_delay_ms(1000);
	lcd_clear();
	_delay_ms(1000);
    }
#endif
}
