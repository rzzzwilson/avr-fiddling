///////////////////////////////////////////////////////////////////////////////
// Implementation of Adafruit_FRAM_I2C.h for the AVR platform.
///////////////////////////////////////////////////////////////////////////////

#include <stdbool.h>
#include <inttypes.h>

bool fram_begin(uint8_t addr)
{
}

void fram_write8(uint16_t fram_addr, uint8_t value)
{
}

uint8_t fram_read8(uint16_t fram_addr)
{
}

void fram_get_device_id(uint16_t *manufacturer_id, uint16_t *product_id)
{
}
