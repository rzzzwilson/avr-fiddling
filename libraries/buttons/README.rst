Simple code to debounce one or more buttons.

All debounce done in firmware.  Only use internal pullup on button pin.
Button is active low.

This version extends the code in *button_debounce* to allow configuration
of a single pin through a "Btn_init()" function and passing a "PORTx" value
and a pin number.
