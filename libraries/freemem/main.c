// Code to test the Events system.

#include <stdio.h>
#include <avr/io.h>
#include <stddef.h>
#include "lcd_1602.h"
#include "freemem.h"

char buff[18];

int main(void)
{
    // prepare the initial screen
    lcd_init();                     	// Initialization of lcd
    lcd_string("freemem test");         // Write ID string on 1st line of lcd

    sprintf(buff, "%d", freemem());
    lcd_string_xy(1, 0, buff);

    // wait here forever
    while (1)
    {
    }
}
