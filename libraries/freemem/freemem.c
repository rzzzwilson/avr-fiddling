//
// Function to get free RAM size.
//

#include <stddef.h>

ptrdiff_t freemem(void)
{
    extern int __heap_start;
    extern int * __brkval;

    const int stackTop;

    return (ptrdiff_t) (&stackTop - ((__brkval == 0) ? &__heap_start : __brkval));
}
