#include <avr/io.h>
#include "eeprom.h"

void eeprom_write_8(uint16_t address, uint8_t data)
{
    // Wait for completion of previous write
    while (EECR & (1 << EEPE))
        ;

    // Set up address and Data Registers
    EEAR = address;
    EEDR = data;

    // Write logical one to EEMPE
    EECR |= ( 1<< EEMPE);

    // Start eeprom write by setting EEPE
    EECR |= (1 << EEPE);
}

void eeprom_write_16(uint16_t address, uint16_t data)
{
    eeprom_write_8(address, (uint8_t) (data >> 8));
    eeprom_write_8(address+1, (uint8_t) (data & 0xff));
}

uint8_t eeprom_read_8(uint16_t address)
{
    // Wait for completion of previous write
    while (EECR & (1 << EEPE))
        ;
    // Set up address register
    EEAR = address;
    
    // Start eeprom read by writing EERE
    EECR |= (1<<EERE);
    
    // Return data from Data Register
    return EEDR;
}

uint16_t eeprom_read_16(uint16_t address)
{
#ifdef JUNK
    uint16_t result = eeprom_read_8(address);
    result <<= 8;
    result |= eeprom_read_8(address+1);
    return result;
#endif

    return (eeprom_read_8(address) << 8) + eeprom_read_8(address+1);
}
