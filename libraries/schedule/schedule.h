/*
  Schedule interface

  Code to schedule functions at a future time.
 */

#ifndef __SCHEDULE_H__
#define __SCHEDULE_H__


// typedef for user function toschedule
typedef void (*scheduled)(void);

// functions advertised by schedule.c
void sched_init(scheduled abort_handler, int num_events);
void sched_add(unsigned long delay, scheduled func);
void sched_tick(void);
void sched_flush(void);
void sched_dump(void);


#endif
