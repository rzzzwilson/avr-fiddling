/*
 * The interface for teh eeprom code.
 */

#ifndef EEPROM_H
#define EEPROM_H

#include <avr/io.h>

void eeprom_write_8(uint16_t address, uint8_t data);
void eeprom_write_16(uint16_t address, uint16_t data);
uint8_t eeprom_read_8(uint16_t address);
uint16_t eeprom_read_16(uint16_t address);

#endif
