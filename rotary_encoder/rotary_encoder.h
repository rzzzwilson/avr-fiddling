// Robust Rotary encoder reading
//
// Define the inteface.
//
// From:
// https://www.best-microcontroller-projects.com/rotary-encoder.html
//
// Much hacked, so any errors are mine!

#ifndef ROTARY_ENCODER_H
#define ROTARY_ENCODER_H

// hardware connections
#define RE_Dir   DDRD   // Define RE data direction port
#define RE_Port  PORTD  // Define RE data port
#define RE_Pin   PIND   // port to read pins from
#define RE_Clock PD2    // Define clock pin
#define RE_Data	 PD3    // Define data pin

// read the RE status
int8_t RE_read(void);

#endif
