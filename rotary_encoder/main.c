// Robust Rotary encoder test code.

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include "rotary_encoder.h"
#include "LCD_1602.h"

int main(void)
{
    int count = 0;
    char buff[32];                  // for LCD output

    // setup CLOCK pin
    RE_Dir &= ~(1 << RE_Clock);     // CLOCK pin is input
    RE_Port |= (1 << RE_Clock);     //     with pullup

    // set up DATA pin
    RE_Dir &= ~(1 << RE_Data);      // DATA pin is input
    RE_Port |= (1 << RE_Data);      //     with pullup

    // prepare the intial screen
    LCD_Init();                     // Initialization of LCD
    LCD_String("RE test code");     // Write string on 1st line of LCD
    sprintf(buff, "count=%d ", count);
    LCD_String_xy(1, 0, buff);      //     and initial "count" value

    // continuously read RE and display results
    while (1)
    {
        int8_t re_val;

        if ((re_val = RE_read()))
        {
            count += re_val;
            sprintf(buff, "count=%d ", count);
            LCD_String_xy(1, 0, buff);
        }
    }
}

