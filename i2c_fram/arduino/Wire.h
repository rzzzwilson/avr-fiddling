///////////////////////////////////////////////////////////////////////////////
// Implementation of the Adafruit Wire.h for the Arduino platorm.
// Designed to be used on AVR platform.
///////////////////////////////////////////////////////////////////////////////

#ifndef _WIRE_H_
#define _WIRE_H_

#include <inttypes.h>
//#include "Stream.h"

#define BUFFER_LENGTH 32


void twi_begin(uint8_t);
void twi_setClock(uint32_t);
void twi_beginTransmission(uint8_t);
uint8_t twi_endTransmission(uint8_t);
uint8_t twi_requestFrom(uint8_t, uint8_t);
//uint8_t twi_requestFrom(uint8_t, uint8_t, uint8_t);
uint8_t twi_write(uint8_t);
//size_t twi_write(const uint8_t *, size_t);
uint8_t twi_available(void);
uint8_t twi_read(void);
uint8_t twi_peek(void);
void twi_flush(void);
void twi_onReceive(void (*)(int));

#endif
