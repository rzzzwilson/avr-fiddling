The classic "blink" test program for an Attiny85.

It is assumed that an LED+resistor is attached between pin PB0 and ground.
Pin PB0 is mapped to physical pin 5 on the Attiny85.

Original code from:

https://balau82.wordpress.com/2011/03/29/programming-arduino-uno-in-pure-c/
