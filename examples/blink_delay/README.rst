The classic "blink" test program.

It is assumed that an LED+resistor is attached between pin PB5 and ground.
Pin PB5 is mapped to physical pin 13 on the Uno (Atmega328P DIP).
Pin PB5 is mapped to physical pin 17 on the Atmega328p-MU (LQFP32).

Original code from:

https://balau82.wordpress.com/2011/03/29/programming-arduino-uno-in-pure-c/
