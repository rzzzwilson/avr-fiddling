Simple code to test a rotary encoder.

Schematic
---------

Circuit used for testing:

.. image:: schematic.png

Works well, with or wthout the 0.1uF capacitors on PD2 and PD3.  Could
try lower capacitance, say 0.01uF or even less.  OK with 0.01uF.

Doesn't work with the 10K "pulldown" resistore shown in the schematic.
Apparently the builtin pullups plus the pulldown holds the pin voltage
too low so we don't get a transition.  47K pulldowns works fine.  Normal
voltage about 1.8v on pin, dropping to GND on switch close.
