// Code to test the millis() function.

#include <stdio.h>
#include <avr/io.h>
//#include <util/delay.h>
#include <avr/interrupt.h>

#include "lcd_1602.h"
#include "millis.h"

char buff[32];

int main(void)
{
    static unsigned long time = 0;

    // prepare the intial screen
    lcd_init();                     	// Initialization of lcd
    lcd_string("millis() test");        // Write string on 1st line of lcd
    lcd_string_xy(1, 0, "0");      	//     and initial "count" value

    // prepare the "millis" code
    millis_init(F_CPU);
    sei();

    // continuously read button and display results
    while (1)
    {
        unsigned long now = millis() / 1000;

	if (now != time)
	{
            sprintf(buff, "%lu", now);
            lcd_string_xy(1, 0, buff);
	    time = now;
	}
    }
}

