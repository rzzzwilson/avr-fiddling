//
// Soft Switch test code
//     LED connected to pin PB5, pin 17, and GND.
//     HOLD is PB0, pin 12
//     SENSE is PB1, pin 13
//


#include <avr/io.h>
#include <util/delay.h>

#define HOLD    PORTB0
#define SENSE   PINB1
#define LED     PORTB5

#define MS_DELAY    500

int main (void)
{
    // set pins for HOLD and LED to output mode
    DDRB |= _BV(DDB0);      // HOLD
    DDRB |= _BV(DDB5);      // LED

    // set HOLD (B0) to HIGH
    PORTB |= _BV(HOLD);

    // configure SENSE pin B1 as input & activate pullup resistor
    DDRB &= ~(1 << SENSE);
    PORTB |= (1 << SENSE);

    while (1)
    {
        // set HIGH the fifth bit of PORTB to one
        PORTB |= _BV(LED);

        // wait a bit
        _delay_ms(MS_DELAY);

        // Set LOW the fifth bit of PORTB
        PORTB &= ~_BV(LED);

        // wait a bit
        _delay_ms(MS_DELAY);

        // check if the button is pressed
        if ((PINB & (1 << SENSE)) == 0)
        {
            // turn on LED
            PORTB |= _BV(LED);

            // wait 2 seconds and it still pressed, power down
            _delay_ms(2000);
            if ((PINB & (1 << SENSE)) == 0)
            {
                // set HOLD to LOW
                PORTB &= ~_BV(HOLD);

                // flash LED and then on permanently
                for (int i = 0; i < 5; ++i)
                {
                    PORTB &= ~_BV(LED);
                    _delay_ms(50);
                    PORTB |= _BV(LED);
                    _delay_ms(50);
                }

                // and then wait forever
                while (1)
                {
                }
            }

            // turn LED off again
            PORTB &= ~_BV(LED);
        }
    }
}
