LCD_1602
========

A library to write to the 1602 LCD display.

Original code from:

https://www.electronicwings.com/avr-atmega/interfacing-lcd-16x2-in-4-bit-mode-with-atmega-16-32-

and the Adafruit LiquidCrystal library.

Test Connections
----------------

The connections for the test code in main.c are::

* Vss     to GND
* Vdd     to +5v
* V0      to variable voltage (0-5)
* RS      to PB4        (chip pin 18)
* RW      to GND
* EN      to PB5        (chip pin 19)
* D4 - D7 to PB0 - PB3  (chip pins 14, 15, 16, 17)
* A       to +5v
* K       to GND
