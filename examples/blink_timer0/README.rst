The classic "blink" test program, but using TIMER0.

It is assumed that an LED+resistor is attached between pin 23 and ground.
Physical 23 is mapped to "23" on the Uno.

Original code from:

https://sites.google.com/site/qeewiki/books/avr-guide/timers-on-the-atmega328

Using the TIMER0 timer.  Blink LED every 4ms.
