#include <stdio.h>
#include "Wire.h"
#include "Adafruit_FRAM_I2C.h"

/* Example code for the Adafruit I2C FRAM breakout */

/* Connect SCL    to analog 5
   Connect SDA    to analog 4
   Connect VDD    to 5.0V DC
   Connect GROUND to common ground */

#define MB85RC_DEFAULT_ADDRESS (0x50) 	// 1010 + A2 + A1 + A0 = 0x50 default
   
//Adafruit_FRAM_I2C fram = Adafruit_FRAM_I2C();

void print(char *msg)
{
}

void print_int(uint8_t i)
{
  char buff[12];
  sprintf(buff, "%d", i);
  print(buff);
}

int main(void)
{
  if (fram_begin(MB85RC_DEFAULT_ADDRESS))
  {
    print("Found I2C FRAM");
  } else
  {
    print("FRAM not found");
//    print("Continuing...");
  }
  
  // Read the first byte
  uint8_t test = fram_read8(0x0);
  print("Reboot ");
  print_int(test);
  // Test write 
  fram_write8(0x0, test+1);

  // wait here forever
  while (1)
  {
  }

  return 0;
}
