This directory contains small test programs that explore the "bare metal"
world of programming an AtMEGA328P.

The *libraries* directory will store "productized" libaries that can be used in
AVR code.

The *examples* directory holds code that isn't a library and should be used as
an example of how to do something.
