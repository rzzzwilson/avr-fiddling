/*
 * Test code to reaad/write EEPROM.
 */

#include <stdio.h> 
#include <avr/io.h>
#include <util/delay.h>
#include "eeprom.h"
#include "lcd_1602.h"


// if defined will minimize memory used in main.c to check library size
//#define CHECK_LIB_SIZE

///////////////////////////////////////////////////////////////////////////////
// Just write a byte to EEPROM and read it back.
///////////////////////////////////////////////////////////////////////////////

int main(void)
{
#ifndef CHECK_LIB_SIZE
    char buff[6];

    lcd_init();                         // Initialization of LCD
    lcd_string_xy(2, 0, "EEPROM test");

    eeprom_write_8(0, 0x45);
    lcd_string_xy(0, 1, "0x45->0");

    uint8_t result = eeprom_read_8(0);
    sprintf(buff, "0->0x%02x", result);
    lcd_string_xy(9, 1, buff);
    _delay_ms(3000);

    lcd_clear();
    eeprom_write_16(1, 0x1234);
    lcd_string("0x1234->1");
    _delay_ms(1000);

    uint16_t result16 = eeprom_read_16(1);
    sprintf(buff, "1->0x%04x", result16);
    lcd_string_xy(0, 1, buff);
    _delay_ms(1000);

#ifdef JUNK
    uint8_t byte = eeprom_read_8(1);
    sprintf(buff, "1->%02x", byte);
    lcd_string_xy(0, 0, buff);
    byte  = eeprom_read_8(2);
    sprintf(buff, "2->%02x", byte);
    lcd_string_xy(0, 1, buff);
#endif

    while (1)
    {
    }
#endif
}
