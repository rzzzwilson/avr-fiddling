Programmer
==========

Use "-c usbtiny" or "-c usbasp" as the programmers.

Fuses
=====

To set fuses on "new" Atmega328P to Arduino standard:

    avrdude -c usbasp -p m328p -b 9600 -U lfuse:w:0xFF:m -U hfuse:w:0xDE:m -U efuse:w:0x05:m

This was with no indication of a functioning clock and no attached crystal.


Reading
-------

To read fuses:

    avrdude -c usbasp -p m328p -v -U hfuse:r:hfuse.hex:i
    avrdude -c usbasp -p m328p -v -U lfuse:r:lfuse.hex:i
    avrdude -c usbasp -p m328p -v -U efuse:r:efuse.hex:i

Standard fuses for arduino are:

    low_fuses=0xff
    high_fuses=0xde
    extended_fuses=0x05

To burn the standard Arduino fuses:

    avrdude -c usbasp -p m328p -v -U hfuse:w:0xde:m -U lfuse:w:0xff:m -U efuse:w:0x05:m

This actually fails setting "efuse"?!


Bricked
-------

After fiddling, got a chip with fuses set to 00, 00, 00.  Clock seems to be running
at around 13.6KHz (on pin 14).  CAN'T FIX (YET)!

Will build a HV programmer to fix hopefully all of the "bricked" chips.

New Atmega328p-AU (LQFP-32)
---------------------------

Connect power+ground with decoupling capacitors.  Add 10K pullup on RESET.
NO CRYSTAL+CAPACITORS!  

    pin 3       GND
    pin 4       Vcc decoupled
    pin 5       GND
    pin 6       Vcc decoupled
    pin 7       xtal (not at first)
    pin 8       xtal (not at first)
    pin 18      Vcc decoupled
    pin 21      GND
    pin 29      RESET 10K pullup

USBTiny connections

    GND         GND
    Vcc         Vcc
    MOSI        pin 15
    MISO        pin 16
    SCK         pin 17
    RST         pin 29

Use Arduino IDE, select "Arduino UNO", "USBtiny" programmer,
and then burn bootloader.
