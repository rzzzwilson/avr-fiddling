A test program to check the "Soft Power Switch 1.0".

It is assumed that an LED+resistor is attached between pin PB5 and ground.
Pin PB5 is mapped to physical pin 13 on the Uno (Atmega328P DIP).
Pin PB5 is mapped to physical pin 17 on the Atmega328p-MU (LQFP32).

The code sets HOLD (PB0, pin 12) high on startup.  This latches the 
Soft Switch ON.  The code blinks the LED on PB5 while checking for a
button press on the SENSE pin (PB1, pin 13).  When PB1 goes low the
code sets HOLD low and changes the blink on the LED to show "shutting down"
and then waits for the PB1 value to go high.
