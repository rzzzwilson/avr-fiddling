#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <util/twi.h>
#include "fram_i2c.h"


// the I2C address of slave we are talking to
uint8_t i2c_addr = 0;


///////////////////////////////////////////////////////////
// Sets up the hardware and initializes I2C.
//
//     addr  The I2C address to be used
//
// Returns true if initialization was successful, otherwise false.
///////////////////////////////////////////////////////////

bool fram_begin(uint8_t addr)
{
    i2c_addr = addr;
    Wire.begin();

    /* Make sure we're actually connected */
    uint16_t manufID;
    uint16_t prodID;

    getDeviceID(&manufID, &prodID);
    if (manufID != 0x00A)
    {
//        Serial.print("Unexpected Manufacturer ID: 0x");
//        Serial.println(manufID, HEX);
        return false;
    }
    if (prodID != 0x510)
    {
//        Serial.print("Unexpected Product ID: 0x");
//        Serial.println(prodID, HEX);
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////
// Writes a byte at the specific FRAM address.
//
//     framAddr  The 16-bit address to write to in FRAM memory
//     value     The 8-bit value to write at framAddr
///////////////////////////////////////////////////////////

void fram_write8(uint16_t framAddr, uint8_t value)
{
    Wire.beginTransmission(i2c_addr);
    Wire.write(framAddr >> 8);
    Wire.write(framAddr & 0xFF);
    Wire.write(value);
    Wire.endTransmission();
}

///////////////////////////////////////////////////////////
// Reads an 8 bit value from the specified FRAM address.
//
//     framAddr  The 16-bit address to write to in FRAM memory
//
// Returns the 8-bit value retrieved at framAddr.
///////////////////////////////////////////////////////////

uint8_t fram_read8(uint16_t framAddr)
{
  Wire.beginTransmission(i2c_addr);
  Wire.write(framAddr >> 8);
  Wire.write(framAddr & 0xFF);
  Wire.endTransmission();

  Wire.requestFrom(i2c_addr, (uint8_t)1);

  return Wire.read();
}

///////////////////////////////////////////////////////////
// Reads the Manufacturer ID and the Product ID from the IC.
//
//     manufacturerID  address of place to store 12bit manufacturer ID
//                     (Fujitsu = 0x00A)
//     productID       address of place to store The memory density (bytes 11..8)
//                     and proprietary Product ID fields (bytes 7..0).
//                     (MB85RC256V = 0x510)
///////////////////////////////////////////////////////////

void get_id(uint16_t *manufacturerID, uint16_t *productID)
{
  uint8_t a[3] = {0, 0, 0};
  uint8_t results;

  Wire.beginTransmission(MB85RC_SLAVE_ID >> 1);
  Wire.write(i2c_addr << 1);
  results = Wire.endTransmission(false);

  Wire.requestFrom(MB85RC_SLAVE_ID >> 1, 3);
  a[0] = Wire.read();
  a[1] = Wire.read();
  a[2] = Wire.read();

  /* Shift values to separate manuf and prod IDs */
  /* See p.10 of
   * http://www.fujitsu.com/downloads/MICRO/fsa/pdf/products/memory/fram/MB85RC256V-DS501-00017-3v0-E.pdf
   */
  *manufacturerID = (a[0] << 4) + (a[1] >> 4);
  *productID = ((a[1] & 0x0F) << 8) + a[2];
}
