#ifndef _FRAM_I2C_H_
#define _FRAM_I2C_H_

#include <stdio.h>
#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>
#include <avr/io.h>

//#include <Arduino.h>
//#include <Wire.h>

#define MB85RC_DEFAULT_ADDRESS (0x50)	// 1010 + A2 + A1 + A0 = 0x50 default
#define MB85RC_SLAVE_ID (0xF8) 		// SLAVE ID

bool fram_begin(uint8_t addr);
void fram_write8(uint16_t framAddr, uint8_t value);
uint8_t fram_read8(uint16_t framAddr);
void fram_get_id(uint16_t *manufacturerID, uint16_t *productID);

#endif
