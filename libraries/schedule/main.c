/*
  Code to exercise the "schedule" module.

  There are three LEDs on pins PD2, PD3 and PD4.
  Turn them on and off at different times without using _delay_ms().
 */

#include <stdint.h>
#include <stdio.h>
#include <stddef.h>
#include <avr/io.h>
#include <util/delay.h>

#include "lcd_1602.h"
#include "millis.h"
#include "schedule.h"


// ports controlling LEDs
#define LED_PORT	PORTD
#define LED_DDR		DDRD

// pins controlling the LEDa
const int led0 = PD7;
const int led1 = PD2;
const int led2 = PD3;
const int led3 = PD4;

// periods for each LED (miliseconds) (primes)
const int led0_period = 97;
const int led1_period = 229;
const int led2_period = 487;
const int led3_period = 1087;

#define LED_MASK	((1 << led0) + (1 << led1) + (1 << led2) + (1 << led3))

//======================================================================
// If the schedule code aborts, hang here.
//======================================================================

void sched_abort(void)
{
    lcd_string_xy(1, 0, "ABORT!");

    LED_PORT |= LED_MASK;	// turn on all LEDs

    while (1)
    {
        LED_PORT ^= LED_MASK;	// toggle all together
        _delay_ms(250);
    }
}

//======================================================================
// Functions to be scheduled.
//======================================================================

void led0_toggle(void)
{
    LED_PORT ^= (1 << led0);
    sched_add(led0_period, led0_toggle);
}

void led1_toggle(void)
{
    LED_PORT ^= (1 << led1);
    sched_add(led1_period, led1_toggle);
}

void led2_toggle(void)
{
    LED_PORT ^= (1 << led2);
    sched_add(led2_period, led2_toggle);
}

void led3_toggle(void)
{
    LED_PORT ^= (1 << led3);
    sched_add(led3_period, led3_toggle);
//    sched_add(led3_period, led3_toggle);	// TESTING, crashes the schedule system after about 2.5s
    						// sched_abort() called
}

//======================================================================
// Flash the three LEDs to indicate the program is starting, then
// schedule the three ON functions.
//======================================================================

void start_led(void)
{
    // exercise all LEDs as a signon test
    LED_PORT |= LED_MASK;
    _delay_ms(500);

    // start all LED colour cycles at same time after a delay
    sched_add(led0_period, led0_toggle);
    sched_add(led1_period, led1_toggle);
    sched_add(led2_period, led2_toggle);
    sched_add(led3_period, led3_toggle);
}

//======================================================================
// Main program.  Initialize everything then just call sched_tick();
//======================================================================

char buff[20];

int main(void)
{
    // start timeing system
    millis_init(16000000L);

    // prepare the LCD display
    lcd_init();
    lcd_string_xy(0, 0, "Schedule test");

    // initialize the LED pins as OUTPUT
    LED_DDR |= LED_MASK;

    // prepare the schedule system, 5 entries, only need 3
    sched_init(sched_abort, 5);

    // start the LEDs
    start_led();

    while (1)
    {
        double time = ((double) millis()) / 1000.;
        sprintf(buff, "%.1fs", time); 
//        sprintf(buff, "%ds", millis()/1000); 
        lcd_string_xy(1, 0, buff);

        sched_tick();   // kick the schedule system along
    }
}
