/*
  Schedule

  Code to handle scheduling of functions to execute in the future.
 */

#include <stdio.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include "millis.h"
#include "schedule.h"

// max allowed value for the scheduled delay (half max overflow period)
#define MAX_WAIT  0x8FFFFFFF

// definition of an ScheduleTable struct
typedef struct { unsigned long when;      // when event scheduled
                 scheduled func;          // function executed when event triggers
               } ScheduleTable;

// stuff to handle the ScheduleTable array
// The array is sorted such that the earlies ScheduleTable is in slot 0,
// the next ScheduleTable is at 1 and so on.
static int NumScheduleTables = 0;           // number of ScheduleTable slots in the system
static int NextSlot = 0;                    // index of first free ScheduleTable slot in "ScheduleTableArray" array
static ScheduleTable *ScheduleTableArray = NULL;    // array of ScheduleTable structs

// address of the user function to call on abort, if given.
static scheduled SchedAbortHandler = NULL;  // address of user "abort" function


//***************************************************************************
// Decision function - return "true" if event should be triggered.
//     now   timestamp of current moment
//     when  timestamp of event execution
//***************************************************************************

bool execute(unsigned long now, unsigned long when)
{
    if (when > now)
        return (when - now) > MAX_WAIT;
    return (now - when) < MAX_WAIT;
}

//***************************************************************************
// Routine to initialize the "schedule" system.
//     abort_handler  address of handler function on abort
//     num_events     required slots in the ScheduleTableArray array
// If "abort_handler" is NULL the handler will not be called on abort.
//***************************************************************************

void sched_init(scheduled abort_handler, int num_events)
{
    NextSlot = 0;
    SchedAbortHandler = abort_handler;
    NumScheduleTables = num_events;
  
    if (ScheduleTableArray != NULL)     // if called twice free previously allocated memory
    {
        free(ScheduleTableArray);
    }

    ScheduleTableArray = (ScheduleTable *) malloc(sizeof(ScheduleTable) * NumScheduleTables);
}

//***************************************************************************
// Internal function used to remove a single slot from the ScheduleTableArray array.
//     slot  index of ScheduleTable to remove
// The ScheduleTables above the removed one are moved down.
//***************************************************************************

static void sched_remove(unsigned int slot)
{
    // if slot to be removed ISN'T at the top of the array we must move slots
    if (slot != (unsigned int) (NextSlot - 1))
    {
        memmove(&ScheduleTableArray[slot], &ScheduleTableArray[slot+1], sizeof(ScheduleTable) * (NextSlot - slot - 1));
    }
  
    --NextSlot;
}

//***************************************************************************
// Routine to flush events from the "schedule" system.
//
// All events are removed from the system, abort handler and table size
// remain unchanged.
//***************************************************************************

void sched_flush(void)
{
    NextSlot = 0;
}

//***************************************************************************
// Routine to schedule an ScheduleTable in the system.
//     wait     delay in ms to wait from now before event is triggered
//     handler  address of handler function to call when event is triggered
//***************************************************************************

void sched_add(unsigned long wait, scheduled handler)
{
    if (wait > MAX_WAIT)
    {
        return;
    }
  
    // check if there are any free slots, if not call abort handler, if defined
    if (NextSlot >= NumScheduleTables)
    {
        if (SchedAbortHandler != NULL)
        {
            SchedAbortHandler();
	}

        return;
    }
  
    // sample the current time NOW
    unsigned long when = millis() + wait;
    int slot;
  
    // if events are empty, just put new at index 0
    if (NextSlot == 0)
    {
        slot = 0;
    }
    else
    {
        for (slot=0; slot < NextSlot; ++slot)
        {
            // complicated by need to handle unsigned long values
            if (!execute(when, ScheduleTableArray[slot].when))
            {
                break;
            }
        }
  
      // "slot" is the index to place to put new ScheduleTable
      if (slot < NextSlot)
      {
          // need to move slots to make room
          memmove(&ScheduleTableArray[slot+1], &ScheduleTableArray[slot], sizeof(ScheduleTable)*(NextSlot - slot));
      }
    }
    
    ScheduleTableArray[slot].when = when;
    ScheduleTableArray[slot].func = handler;

    ++NextSlot;
}

//***************************************************************************
// The "tick" function.  Should be called periodically in user code to check
// if an event needs to be triggered.
//***************************************************************************

void sched_tick(void)
{
    unsigned long now = millis();
    int slot;                       // found slot index

    // look for first slot with non-due event
    for (slot = 0; slot < NextSlot; ++slot)
    {
        // complicated comparison to handle millis() overflow (?)
        if (!execute(now, ScheduleTableArray[slot].when))
        {
            break;
        }
    }
 
    if (slot > 0)
    {
        for (int i = 0; i < slot; ++i)
        {
            scheduled what = ScheduleTableArray[0].func;   // get handler for earliest ScheduleTable
        
            // remove the slot just actioned BEFORE calling handler, which *might* do sched_sched()!
            sched_remove(0);
  
            what();
        }
    }
}
