// Code to implement the arduino millis() function.
//
// Warning: This code uses TIMER1.
//
// From:
// https://github.com/monoclecat/avr-millis-function
//
// Much hacked, so any errors are mine!

#include <stdio.h>
#include <avr/io.h>
#include <util/atomic.h>
#include <avr/interrupt.h>
#include "millis.h"


volatile unsigned long timer1_millis;

// interrupt handler for Timer1

ISR(TIMER1_COMPA_vect)
{
    timer1_millis++;
}

void millis_init(unsigned long f_cpu)
{
    unsigned long ctc_match_overflow;

    ctc_match_overflow = ((f_cpu / 1000) / 8); //when timer1 is this value, 1ms has passed

    // (Set timer to clear when matching ctc_match_overflow) | (Set clock divisor to 8)
    TCCR1B |= (1 << WGM12) | (1 << CS11);

    // high byte first, then low byte
    OCR1AH = (ctc_match_overflow >> 8);
    OCR1AL = ctc_match_overflow;

    // Enable the compare match interrupt
    TIMSK1 |= (1 << OCIE1A);

    //REMEMBER TO ENABLE GLOBAL INTERRUPTS AFTER THIS WITH sei(); !!!
}

//-------------------------------------
// Get the number of milliseconds since boot.
// Returns:
//     uint16_t values holding the millisecond count
//-------------------------------------

unsigned long millis(void)
{
    unsigned long millis_return;
 
    // Ensure this cannot be disrupted
    ATOMIC_BLOCK(ATOMIC_FORCEON)
    {
        millis_return = timer1_millis;
    }

    return millis_return;
}
