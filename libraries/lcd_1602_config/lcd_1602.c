/*
 * Library to control the 1602 LCD display.
 *
 * Allows configuration through #defines below.
 * All 4 data pins must be on same port, any order.
 */

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "lcd_1602.h"

// if defined, compile code to check ranges of some params
//#define CHECK_PARAM

//******
// Define connections to the display, number of lines, etc.
//******

#define PORT_RS         PORTB
#define PIN_RS          PB4

#define PORT_EN         PORTB
#define PIN_EN          PB5

#define PORT_DATA       PORTB	// all data pins must be in this port
#define PIN_D4          PB0	// but the pins can be in any order
#define PIN_D5          PB1
#define PIN_D6          PB2
#define PIN_D7          PB3

// number of lines in display
#define NUMLINES	2	// only the 1602 display (2 lines of 16 chars each)

//******
// defines to indicate type of data in "lcd_write()" - command or data
//******
 
#define WRITE_CMD       0
#define WRITE_DATA      1

//******
// Commands for the 1602 display (taken from Adafruit library)
//******

#define LCD_CLEARDISPLAY    0x01 	// Clear display, set cursor position to zero
#define LCD_RETURNHOME      0x02 	// Set cursor position to zero
#define LCD_ENTRYMODESET    0x04 	// Sets the entry mode
#define LCD_DISPLAYCONTROL  0x08 	// Controls the display; does stuff like turning it off and on
#define LCD_CURSORSHIFT     0x10 	// Lets you move the cursor
#define LCD_FUNCTIONSET     0x20 	// Used to send the function to set to the display
#define LCD_SETCGRAMADDR    0x40 	// Used to set the CGRAM (character generator RAM) with characters
#define LCD_SETDDRAMADDR    0x80 	// Used to set the DDRAM (Display Data RAM)

// flags for display entry mode
#define LCD_ENTRYRIGHT      0x00 	// Used to set text to flow from right to left
#define LCD_ENTRYLEFT       0x02 	// Uset to set text to flow from left to right
#define LCD_ENTRYSHIFTINCREMENT  0x01 	// Used to 'right justify' text from the cursor
#define LCD_ENTRYSHIFTDECREMENT  0x00 	// Used to 'left justify' text from the cursor

// flags for display on/off control
#define LCD_DISPLAYON       0x04 	// Turns the display on
#define LCD_DISPLAYOFF      0x00 	// Turns the display off
#define LCD_CURSORON        0x02 	// Turns the cursor on
#define LCD_CURSOROFF       0x00 	// Turns the cursor off
#define LCD_BLINKON         0x01 	// Turns on the blinking cursor
#define LCD_BLINKOFF        0x00 	// Turns off the blinking cursor

// flags for display/cursor shift
#define LCD_DISPLAYMOVE     0x08 	// Flag for moving the display
#define LCD_CURSORMOVE      0x00 	// Flag for moving the cursor
#define LCD_MOVERIGHT       0x04 	// Flag for moving right
#define LCD_MOVELEFT        0x00 	// Flag for moving left

// flags for function set
#define LCD_8BITMODE        0x10 	// LCD 8 bit mode
#define LCD_4BITMODE        0x00 	// LCD 4 bit mode
#define LCD_2LINE           0x08 	// LCD 2 line mode
#define LCD_1LINE           0x00 	// LCD 1 line mode
#define LCD_5x10DOTS        0x04 	// 10 pixel high font mode
#define LCD_5x8DOTS         0x00 	// 8 pixel high font mode

//******
// State variables.
//******

// initial state of the display: display on/off, cursor on/off, blink on/off
static uint8_t DisplayControl = LCD_DISPLAYON | LCD_CURSOROFF | LCD_BLINKOFF;

// array of data pin offsets (D4, D5, D6, D7 order, from the left)
// this is so we can interate through "pin_offset" and write low bit, then shift
static uint8_t pin_offset[] = {(1 << PIN_D4), (1 << PIN_D5), (1 << PIN_D6), (1 << PIN_D7)};


///////////////////////////////////////////////////////////////////////////////
// Pulse the EN bit high for >450ns
///////////////////////////////////////////////////////////////////////////////

#ifdef JUNK
static void lcd_pulse_en(void)
{
    PORT_EN |= (1 << PIN_EN);   // EN high
    _delay_us(1);               // 1000ns delay
    PORT_EN &= ~(1 << PIN_EN);  // EN low
}
#endif

// save a few bytes or call overhead by inlining this
#define lcd_pulse_en()  {PORT_EN |= (1 << PIN_EN); _delay_us(1); PORT_EN &= ~(1 << PIN_EN);}

///////////////////////////////////////////////////////////////////////////////
// Write 4 bits of data to the display.
// Data to write is in the low 4 bits of "data".
///////////////////////////////////////////////////////////////////////////////

static void lcd_write_4bits(uint8_t data)
{
    for (int i = 0; i < 4; ++i)         // unwinding this loop doesn't save
    {
        if (data & 0x01)
            PORT_DATA |= pin_offset[i];
	else
            PORT_DATA &= ~pin_offset[i];
        data >>= 1;
    }

    lcd_pulse_en();                     // perform the write
}

///////////////////////////////////////////////////////////////////////////////
// Write to the LCD, either a command (rs=WRITE_CMD) or data (rs = WRITE_DATA).
///////////////////////////////////////////////////////////////////////////////

static void lcd_write(uint8_t cmd, uint8_t rs)
{
    if (rs == WRITE_DATA)
        PORT_RS |= (1 << PIN_RS);   // RS=1, data write
    else
        PORT_RS &= ~(1 << PIN_RS);  // RS=0, command write

    lcd_write_4bits(cmd >> 4);      // write high nybble,
    lcd_write_4bits(cmd & 0x0F);    //     then low

    _delay_us(50);                  // commands need > 37us to settle
}

///////////////////////////////////////////////////////////////////////////////
// Initialize the LCD.
///////////////////////////////////////////////////////////////////////////////

void lcd_init(void)
{
    _delay_ms(50);                      // LCD Power ON delay, uC starts faster

    // set the required pins as OUTPUT
    *(&PORT_RS - 1) |= (1 << PIN_RS);   // (&PORT_xx - 1) gets DDRx register
    *(&PORT_EN - 1) |= (1 << PIN_EN);

    // used to loop over "pin_offset[]", but smaller code if loop unwound
    *(&PORT_DATA - 1) |= (1 << PIN_D4);
    *(&PORT_DATA - 1) |= (1 << PIN_D5);
    *(&PORT_DATA - 1) |= (1 << PIN_D6);
    *(&PORT_DATA - 1) |= (1 << PIN_D7);

    // initialize the display
    lcd_write(0x02, WRITE_CMD);     // send for 4 bit initialization of LCD
    lcd_write(0x28, WRITE_CMD);     // 2 line, 5*7 matrix in 4-bit mode
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);  // display on, no cursor
    lcd_write(LCD_ENTRYMODESET | LCD_ENTRYLEFT, WRITE_CMD);     // left-to-right text
    lcd_clear();
}

///////////////////////////////////////////////////////////////////////////////
// Clear the LCD display.
///////////////////////////////////////////////////////////////////////////////

void lcd_clear(void)
{
    lcd_write(LCD_CLEARDISPLAY, WRITE_CMD);
    _delay_ms(2);
}

///////////////////////////////////////////////////////////////////////////////
// Move the cursor to 0,0.
///////////////////////////////////////////////////////////////////////////////

void lcd_home(void)
{
    lcd_write(LCD_RETURNHOME, WRITE_CMD);
    _delay_ms(2);
}

///////////////////////////////////////////////////////////////////////////////
// Send character to LCD.
///////////////////////////////////////////////////////////////////////////////

void lcd_char(uint8_t ch)
{
    lcd_write(ch, WRITE_DATA);
}

///////////////////////////////////////////////////////////////////////////////
// Send string to LCD.
///////////////////////////////////////////////////////////////////////////////

void lcd_string (char *str)
{
    while (*str)
        lcd_write(*str++, WRITE_DATA);
}

///////////////////////////////////////////////////////////////////////////////
// Set cursor position.
//     col  column position
//     row  row position
///////////////////////////////////////////////////////////////////////////////

void lcd_set_cursor(uint8_t col, uint8_t row)
{
    uint8_t offsets[] = {0x00, 0x40};   // only 2 lines on this device

#ifdef CHECK_PARAM
    if (row > (NUMLINES - 1))
        row = NUMLINES;
#endif

    lcd_write(LCD_SETDDRAMADDR | (col + offsets[row]), WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Send string to LCD with xy position.
//     row  row position
//     col  column position
//     str  the string to display
///////////////////////////////////////////////////////////////////////////////

void lcd_string_xy(uint8_t col, uint8_t row, char *str)
{
    lcd_set_cursor(col, row);	// set cursor to required position
    lcd_string(str);            // Call LCD string function
}

///////////////////////////////////////////////////////////////////////////////
// Turn display on or off.
///////////////////////////////////////////////////////////////////////////////

void lcd_display_on(void)
{
    DisplayControl |= LCD_DISPLAYON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

void lcd_display_off(void)
{
    DisplayControl &= ~LCD_DISPLAYON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Turn the "underline" cursor on or off.
///////////////////////////////////////////////////////////////////////////////

void lcd_cursor_on(void)
{
    DisplayControl |= LCD_CURSORON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

void lcd_cursor_off(void)
{
    DisplayControl &= ~LCD_CURSORON;
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);
}

///////////////////////////////////////////////////////////////////////////////
// Turn blinking cursor on or off.
// This should not be confused with the "underline" cursor.
// The blinking cursor and the underline cursor can be combined,
///////////////////////////////////////////////////////////////////////////////

void lcd_blink_on(void)
{
    DisplayControl |= LCD_BLINKON;                              // set BLINK bit ON
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);  // reset mode
}

void lcd_blink_off(void)
{
    DisplayControl &= ~LCD_BLINKON;                             // set BLINK bit OFF
    lcd_write(LCD_DISPLAYCONTROL | DisplayControl, WRITE_CMD);  // reset mode
}

///////////////////////////////////////////////////////////////////////////////
// Create a custom CG character.
//     index     is index (0-7) of character we are defining
//     chardata  pointer to uint8_t data bytes
///////////////////////////////////////////////////////////////////////////////

void lcd_create_char(uint8_t index, uint8_t *chardata)
{
#ifdef CHECK_PARAM
    index &= 0x7;				// we only have 8 nums (0-7)
#endif

    lcd_write(LCD_SETCGRAMADDR | (index << 3), WRITE_CMD);
    for (int i=0; i < 8; ++i)
        lcd_write(*chardata++, WRITE_DATA);
}

///////////////////////////////////////////////////////////////////////////////
// Scroll whole display left and right.
///////////////////////////////////////////////////////////////////////////////

void lcd_scroll_left(void)
{
    lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVELEFT, WRITE_CMD);
}

void lcd_scroll_right(void)
{
    lcd_write(LCD_CURSORSHIFT | LCD_DISPLAYMOVE | LCD_MOVERIGHT, WRITE_CMD);
}
