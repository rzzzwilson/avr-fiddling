// Interface to the "free memory" function.

#ifndef FREEMEM_H
#define FREEMEM_H

#include <stddef.h>

ptrdiff_t freemem(void);

#endif
