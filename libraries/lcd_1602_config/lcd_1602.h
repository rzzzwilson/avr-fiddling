/*
 * Interface for the 1602 LCD display.
 */

#ifndef __LCD_1602_H__
#define __LCD_1602_H__

// public functions in the library
void lcd_init(void);					// Initialize the LCD.
void lcd_clear(void);					// Clear the LCD display.
void lcd_home(void);					// Move the cursor to 0,0
void lcd_char(uint8_t data);				// Send character to LCD, move cursor
void lcd_string(char *str);				// Send string to LCD, move cursor
void lcd_string_xy(uint8_t row, uint8_t col, char *str);// Send string to LCD with xy position, move cursor
void lcd_set_cursor(uint8_t col, uint8_t row);		// set cursor position
void lcd_display_on(void);				// turn display ON
void lcd_display_off(void);				// turn display OFF
void lcd_cursor_on(void);				// turn cursor ON
void lcd_cursor_off(void);				// turn cursor OFF
void lcd_blink_on(void);				// turn blink ON
void lcd_blink_off(void);				// turn blink OFF
void lcd_create_char(uint8_t num, uint8_t *data);	// create new CG character
void lcd_scroll_left(void);				// scroll display one place left
void lcd_scroll_right(void);				// scroll display one place right

#endif
