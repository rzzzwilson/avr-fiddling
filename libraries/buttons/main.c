// Code to test debounce of two buttons.

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>

#include "lcd_1602.h"
#include "buttons.h"

char buff[32];

void handler(void)
{
    static uint16_t count = 0;

    ++count;
    sprintf(buff, "D4=%d", count);
    lcd_string_xy(1, 0, buff);
}

void handler2(void)
{
    static uint16_t count = 0;

    ++count;
    sprintf(buff, "D2=%d", count);
    lcd_string_xy(1, 8, buff);
}

int main(void)
{
    // prepare the intial screen
    lcd_init();                     	// Initialization of LCD
    lcd_string("Button test code");     // Write ID string on 1st line of LCD
    lcd_string_xy(1, 0, "D4=0");
    lcd_string_xy(1, 8, "D2=0");

    // initialize the button code
    Btn_connect(&PORTD, PIND4, &handler);
    Btn_connect(&PORTD, PIND2, &handler2);

    // continuously read buttons and display results
    while (1)
    {
        Btn_read();
    }
}

