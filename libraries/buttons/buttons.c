// Code to debounce one or more "active low" buttons.
// Buttons on the given port+pin have internal pullups activated
// and button must connect pin to GND.
//
// From: http://www.ganssle.com/debouncing-pt2.htm
// Much hacked, so any errors are mine!
//
// Handles many (up to NUM_BUTTONS) port+pin inputs.

#include <stdio.h>
#include <avr/io.h>
#include <util/delay.h>
#include "buttons.h"

typedef struct
{
    volatile uint8_t *pinx;     // pointer to the PINx register
    uint8_t pin;                // the pin number
    uint8_t read_state;         // last 8 bits of the button raw reads
    uint8_t logical_state;      // button logical state, 0 (UP) or 1 (DOWN)
    BTN_HANDLER *handler;       // the handler function for this button
} Button;

Button BtnSlots[NUM_BUTTONS];
uint8_t NextSlot = 0;

//-------------------------------------
// Prepare a port+pin combination to handle an active low button.
//     portx    address of the appropriate port (ie, &PORTD)
//     pin      the number of port pin attached to the button
//     handler  address of the handler function called on PINUP
//              (rising edge)
//
// Returns a small integer index associated with the pin+function.
// Returns -1 if handler table full.
//-------------------------------------

int8_t Btn_connect(volatile uint8_t *portx, uint8_t pin, BTN_HANDLER *handler)
{
    // do nothing if no more free slots
    if (NextSlot >= NUM_BUTTONS)
        return -1;

    // get pointer to "Button" slot to fill
    Button *slot = &BtnSlots[NextSlot];

    // save the slot data
    slot->pinx = portx - 2;     // save the PINx address to read the port data from
    slot->pin = pin;            // pin number in the port
    slot->read_state = 0;       // last 8 bits of physical button state
    slot->logical_state = 0;    // logical button state, 0 (UP) and 1 (DOWN)
    slot->handler = handler;    // handler function to call on pin UP

    // establish port+pin as input and using pullup
    *(portx - 1) &= ~(1 << pin);        // set pin as INPUT
    *portx |= (1 << pin);               //     and as PULLUP

    // return slot integer and move pointer to next free slot
    return NextSlot++;
}

//-------------------------------------
// Read the button associated with an index.
//
// Calls the handler associated with the button when the button is released.
//-------------------------------------

void Btn_read_index(uint8_t index)
{
    // if index not in use do nothing
    if (index < 0 || index >= NextSlot)
        return;

    // otherwise, prepare to read port+pin
    Button *slot = &BtnSlots[index];	// get pointer to "Button" slot to use

    volatile uint8_t *pinx = slot->pinx;
    uint8_t pin = slot->pin;
    uint8_t read_state = slot->read_state;
    uint8_t logical_state = slot->logical_state;

    // get port+pin current value
    uint8_t value = (*pinx & (1 << pin)) >> pin;

    // fit pin value into "last 8" buffer, save back to slot for next time
    read_state = (read_state << 1) | value | 0xe0;
    slot->read_state = read_state;

    if (read_state == 0xe0)
    {   // button is down, just set the state
        if (logical_state == 0)
        {   // state was UP, now DOWN, save DOWN state
            slot->logical_state = 1;
        }
    }
    else
    {   // button is up, if this is first UP reading after DOWN then call handler
        if (logical_state == 1)
        {
            slot->logical_state = 0;    // set button logical state to UP
            (*slot->handler)();         // & call the handler
        }
    }
}

//-------------------------------------
// Read all buttons.
//
// Call the handler associated with each button on button RELEASE.
//-------------------------------------

void Btn_read(void)
{
    for (uint8_t index = 0; index < NextSlot; ++index)
    {
        Btn_read_index(index);
    }
}
