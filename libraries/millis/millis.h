// Code to implement the arduino millis() function.
//
// Define the inteface.
//
// Warning: This code uses TIMER1.
//
// From:
// https://github.com/monoclecat/avr-millis-function
//
// Much hacked, so any errors are mine!

#ifndef MILLIS_H
#define MILLIS_H

void millis_init(unsigned long freq);	// initialize the millis() code
unsigned long millis(void);		// read the current milliseconds since boot

#endif
