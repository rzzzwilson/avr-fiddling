//
// Test code - LED connected to pin PB5 and GND.
//

#include <avr/io.h>
#include <util/delay.h>

#define MS_DELAY 100

int main (void)
{
    /* Set to one the fifth bit of DDRB to one
     * Set digital pin B5 (pin 6) to output mode */
    DDRB |= _BV(DDB5);

    while (1)
    {
        /*Set to one the fifth bit of PORTB to one
        **Set to HIGH the pin 6 */
        PORTB |= _BV(PORTB5);

        /*Wait 3000 ms */
        _delay_ms(MS_DELAY);

        /*Set to zero the fifth bit of PORTB
        **Set to LOW the pin 6 */
        PORTB &= ~_BV(PORTB5);

        /*Wait 3000 ms */
        _delay_ms(MS_DELAY);
    }
}
