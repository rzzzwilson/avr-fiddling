// Code to test the Events system.

#include <stdio.h>
#include <avr/io.h>

#include "lcd_1602.h"
#include "events.h"

char buff[32];

// define some Events
enum TestEvents
{
    Event_A = 1,
    Event_B,
    Event_C
};

// number of error column
uint8_t col = 0;

void error(char *msg)
{
    lcd_string_xy(1, col++, msg);
}

int main(void)
{
    // prepare the initial screen
    lcd_init();                     	// Initialization of lcd
    lcd_string("Events test code");     // Write ID string on 1st line of lcd

    EVENT result;

    event_add(Event_A);
    event_add(Event_B);
    event_add(Event_C);
    event_add(Event_B);
    result = event_get();
    if (result != Event_A)
        error("0");
    else
        error(".");
    event_add(Event_A);
    result = event_get();
    if (result != Event_B)
        error("1");
    else
        error(".");
    result = event_get();
    if (result != Event_C)
        error("2");
    else
        error(".");
    result = event_get();
    if (result != Event_B)
        error("3");
    else
        error(".");
    result = event_get();
    if (result != Event_A)
        error("4");
    else
        error(".");
    result = event_get();
    if (result != Event_None)
        error("5");
    else
        error(".");

    lcd_string_xy(1, 15, "*");

    // wait here forever
    while (1)
    {
    }
}
