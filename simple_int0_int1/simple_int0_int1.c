/*
 * Code to to test the INT0 and INT1 pins and interrupt handling.
 * Touching ground to either of INT0 or INT1 will light the appropriate
 * LED for a short time.
 *
 * Uses two LEDs connected to pins PB0 and PB1 to show things.
 */

#include <stdio.h> 
#include <stdbool.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>


volatile bool int0_flag = false;    // "true" if interrupt occurred
volatile bool int1_flag = false;

void flash(void)
{
    PORTB |= (1 << PB0);
    _delay_ms(100);
    PORTB &= ~(1 << PB0);
    _delay_ms(100);
}

ISR (INT0_vect)
{
    int0_flag = true;
}

ISR (INT1_vect)
{
    int1_flag = true;
}

int main(void)
{
    // DB0 and DB1 as output, both OFF
    DDRB |= (1 << DDB0);
    DDRB |= (1 << DDB1);
    PORTB &= ~(1 << PB0);
    PORTB &= ~(1 << PB1);

    flash();
    flash();
    flash();

    // setup PD2 (INT0)
    DDRD &= ~(1 << DDD2);	// PD2 is input
    PORTD |= (1 << PORTD2);	//     with pullup
    EICRA |= (1 << ISC00);	// set INT0 to trigger on ANY logic change
    EIMSK |= (1 << INT0);	//     and enable INT0 interrupt

    // set up PD3 (INT1)
    DDRD &= ~(1 << DDD3);	// PD3 is input
    PORTD |= (1 << PORTD3);	//     with pullup
    EICRA |= (1 << ISC10);	// set INT1 to trigger on ANY logic change
    EIMSK |= (1 << INT1);	//     and turns on INT1

    sei();			// turn on interrupts

    while (1)
    {
        if (int0_flag)
        {
            PORTB |= (1 << PB0);    // PB0 on
            int0_flag = false;
        }
        if (int1_flag)
        {
            PORTB |= (1 << PB1);    // PB1 on
            int1_flag = false;
        }
        _delay_ms(100);
        PORTB &= ~(1 << PB0);	// both LEDs off
        PORTB &= ~(1 << PB1);
    }
}

