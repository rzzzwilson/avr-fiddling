///////////////////////////////////////////////////////////////////////////////
// Implementation of Adafruit_FRAM_I2C.h for the AVR platform.
///////////////////////////////////////////////////////////////////////////////
//
#ifndef _ADAFRUIT_FRAM_I2C_H_
#define _ADAFRUIT_FRAM_I2C_H_

//#include <Arduino.h>
#include <stdbool.h>
#include "Wire.h"

bool fram_begin(uint8_t addr);
void fram_write8(uint16_t fram_addr, uint8_t value);
uint8_t fram_read8(uint16_t fram_addr);
void fram_get_device_id(uint16_t *manufacturer_id, uint16_t *product_id);

#endif

