eeprom
======

A library to read/write EEPROM values.

Original code:
https://embedds.com/accessing-avr-eeprom-memory-in-avrgcc/
